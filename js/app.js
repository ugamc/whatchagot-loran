// Returns a function, that, as long as it continues to be invoked, will not
// be triggered. The function will be called after it stops being called for
// N milliseconds. If `immediate` is passed, trigger the function on the
// leading edge, instead of the trailing.
function debounce(func, wait, immediate) {
	var timeout;
	return function() {
		var context = this, args = arguments;
		var later = function() {
			timeout = null;
			if (!immediate) func.apply(context, args);
		};
		var callNow = immediate && !timeout;
		clearTimeout(timeout);
		timeout = setTimeout(later, wait);
		if (callNow) func.apply(context, args);
	};
}

jQuery(document).ready(function($){
    $('table.responsive').footable({
        "breakpoints": {
            "mobile": 480,
            "tablet": 769,
            "desktop": 1024,
            "widescreen": 1216,
            "fullhd": 1408
        }
    });
    
    $('.dropdown').on('click', 'button', function() {
        $(this).closest('.dropdown').toggleClass('is-active');
    })

    var initMobileNav = debounce(function() {
        var $win = $(this),
            $headerContent = $('#header-content'),
            $mobileToggle = $('#mobile-toggle');
            
        if ($win.width() < 1088) {
            // add ARIA attributes when mobile
            $headerContent.attr('aria-hidden', 'true');
            $mobileToggle.attr('aria-expanded', 'false').attr('aria-controls', 'header-content');
        } else {
            // remove ARIA attributes when desktop
            $headerContent.removeAttr('aria-hidden');
            $mobileToggle.removeAttr('aria-expanded').removeAttr('aria-controls');
        }
    }, 250);
    
    // add mobile nav on page load
    initMobileNav();
    
    $(window).on('resize', function(){
        initMobileNav();
    });
    
    /* 
     * Submenu Navigation
     */
    // Reusable functions
    function openMenu($current) {
        // $allToggles.attr("aria-expanded", "false");
        $current.attr("aria-expanded", "true");
    }

    function closeMenu($current) {
        $current.attr("aria-expanded", "false");
    }

    function focusSubmenu($current) {
        $current.on("transitionend", function() {
            if ($current.css("visibility") === "visible") {
                $current.find("li:first-child a").focus();
                $current.off("transitionend");
            }
        });
    }
    function toggleSubmenu($toggle) {
        // console.log($toggle);
        $parent = $toggle.parent('a');
        var $submenu = $parent.next(".sub-menu");

        if ($parent.attr("aria-expanded") === "true") closeMenu($parent);
        else openMenu($parent);

        focusSubmenu($submenu);
    }
    
    function toggleMobileHeader() {
        $nav = $('#header-content');
        if ($nav.attr('aria-hidden') === 'true') {
            $nav.attr('aria-hidden', 'false');
        } else {
            $nav.attr('aria-hidden', 'true');
        }
    }

    $('#mobile-toggle').on('click', function() {
        $nav = $('#header-content');
        $this = $(this);
        if ($nav.attr('aria-hidden') === 'true') {
            $this.attr('aria-expanded', 'true');
        } else {
            $this.attr('aria-expanded', 'false');
        }

        toggleMobileHeader();
    });

    $('#header-content').on('click', '.current-menu-item > a', function() {
        toggleMobileHeader();
    });
    
    if ( $( "#masthead" ).length ) {
        var $allMenus = $("#primary-menu > .menu-item-has-children");
        var $allToggles = $allMenus.find('> a');
        var $allTopLinks = $("#primary-menu > li > a");
        var $activeMenu = $allMenus.filter('.current-menu-ancestor, .current-menu-item').find('> a');

        var hoverTimer, blurTimer, 
            delay = 500;

        // Add aria roles
        $(".current-menu-item > a").attr("aria-current", "page");
        $allToggles.attr({
            "aria-haspopup": "true",
            "aria-expanded": "false",
            "role": "button"
        });

        // Open menu on hover
        /*
        $allMenus.on("mouseenter", function(e) {
            openMenu($(this).find("[aria-expanded]"));
    
            clearTimeout(hoverTimer);
        });
        */

        // Close menu after a short delay
        /*
        $allMenus.on("mouseleave", function() {
            var $element = $(this).find("[aria-expanded]");
    
            hoverTimer = setTimeout(function() {
                closeMenu($element);
            }, delay);
        });
        */

        // initialize open menus
        $allToggles.append('<span class="toggle" role="button"><i class="fas fa-fw fa-chevron-right"></i></span>');
        toggleSubmenu($activeMenu.find('.toggle'));

        // Toggle menu on click, tap, or focus + enter/space
        $allToggles
            .on("click touchstart", '.toggle', function(e) {
                toggleSubmenu($(this));

                e.preventDefault();
            })
            .on("keyup", function(e) {
                if (e.keyCode === 32) {
                    openMenu($(this));
                    focusSubmenu($(this).parent('a').next(".sub-menu"));
                }
            });

        // Close menu when refocusing on top-level links
        $allTopLinks.on("focus", function() {
            /*closeMenu($allToggles);*/
        });

        // Close menu on esc and focus loss
        $(".site-navigation").on("keyup", function(e) {
            if (e.keyCode === 27) closeMenu($allToggles);
        });

        // Close menu if focus isn't inside site navigation
        $('.sub-menu').on('focusout', function(){
            // There's a delay between focusout and re-focus
            setTimeout( function() {
                var $focused = $(document.activeElement);
                if($focused.closest('.site-navigation').length === 0 ) {
                    /*closeMenu($allToggles);*/
                }
            }, 1);
        });
    }

    // init quicksearch and button filter values
    var qsRegex;
    var buttonFilter;
    
    // init Isotope
    var $grid = $('.styleguide__list');

    $grid.isotope({
        layoutMode: 'vertical',
        vertical: {
            horizontalAlignment: 0,
        },
        transitionDuration: 0,
        filter: function() {
            var $this = $(this);
            var searchResult = qsRegex ? $this.text().match( qsRegex ) : true;
            var buttonResult = buttonFilter ? $this.hasClass( buttonFilter ) : true;
            return searchResult && buttonResult;
        }
    });
    
    // filter items on button click
    $('#styleguide-categories').on( 'click', 'a', function(e) {
        $this = $(this);
        e.preventDefault();
        if ($this.hasClass('is-active')) {
            $this.removeClass('is-active');
            buttonFilter = false;
            $this.closest('.dropdown').find('.dropdown-trigger .dropdown-label').text('Filter by Category');
        } else {
            $this.addClass('is-active').siblings('a').removeClass('is-active');
            buttonFilter = $this.data('filter');
            // console.log(buttonFilter);            
            $this.closest('.dropdown').find('.dropdown-trigger .dropdown-label').text($this.text());
        }
        $this.closest('.dropdown').removeClass('is-active');
        $grid.isotope();
    });
    
    // use value of search field to filter
    var $quicksearch = $('#styleguide__list-search').keyup( debounce( function() {
        qsRegex = new RegExp( $quicksearch.val(), 'gi' );
        $grid.isotope();
    }, 200 ) );
    
    function zebraStripe() {
        $grid.children('li').removeClass('even odd');
        $grid.children('li:visible:even').addClass('odd');
        $grid.children('li:visible:odd').addClass('even');
    }
    
    zebraStripe();
    
    $grid.on( 'arrangeComplete', zebraStripe );
});