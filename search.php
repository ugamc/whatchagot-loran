<?php
/**
 * The template for displaying search results pages
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#search-result
 *
 * @package Whatchagot_Loran
 */

get_header();
?>

	<div class="section">
        <div id="site__content-area" class="container">
            <main id="primary" class="site__main">

            <?php if ( have_posts() ) : ?>

                <header class="page__header">
                    <h1 class="page__title">
                        <?php
                        /* translators: %s: search query. */
                        printf( esc_html__( 'Search Results for: %s', 'whatchagot-loran' ), '<em>' . get_search_query() . '</em>' );
                        ?>
                    </h1>
                </header><!-- .page-header -->

                <?php
                /* Start the Loop */
                while ( have_posts() ) :
                    the_post();

                    /**
                     * Run the loop for the search to output the results.
                     * If you want to overload this in a child theme then include a file
                     * called content-search.php and that will be used instead.
                     */
                    get_template_part( 'template-parts/content', 'search' );

                endwhile;

                the_posts_navigation();

            else :

                get_template_part( 'template-parts/content', 'none' );

            endif;
            ?>

            </main><!-- #primary -->
            <?php
            get_sidebar();
            ?>
        </div><!-- #site__content-area -->
	</div><!-- .section -->

<?php
get_footer();
