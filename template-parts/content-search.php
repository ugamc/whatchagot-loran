<?php
/**
 * Template part for displaying results in search pages
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package Whatchagot_Loran
 */

?>

<article id="post-<?php the_ID(); ?>" <?php post_class( 'entry entry--search' ); ?>>
	<header class="entry__header">
        <div class="entry__header-content">
            <?php the_title( sprintf( '<h2 class="entry__title"><a href="%s" rel="bookmark">', esc_url( get_permalink() ) ), '</a></h2>' ); ?>
            <?php if ( 'post' === get_post_type() ) : ?>
            <div class="entry__meta">
                <?php
                whatchagot_loran_posted_on();
                whatchagot_loran_posted_by();
                ?>
            </div><!-- .entry__meta -->
            <?php endif; ?>
        </div>
        <?php whatchagot_loran_edit_link(); ?>
	</header><!-- .entry__header -->

	<?php
    // whatchagot_loran_post_thumbnail();
    ?>

	<div class="search-entry__summary content">
		<?php the_excerpt(); ?>
	</div><!-- .search-entry__summary -->

	<footer class="search-entry__footer">
		<?php whatchagot_loran_entry_footer(); ?>
	</footer><!-- .search-entry__footer -->
</article><!-- #post-<?php the_ID(); ?> -->
