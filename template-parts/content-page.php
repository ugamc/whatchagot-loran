<?php
/**
 * Template part for displaying page content in page.php
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package Whatchagot_Loran
 */

?>

<article id="post-<?php the_ID(); ?>" <?php post_class( 'entry entry--page' ); ?>>
    <header class="page__header">
		<?php
        if ( has_post_thumbnail() ) : // check if the post has a Post Thumbnail assigned to it.
            ?>
            <style>
                .page__header-background {
                    background-image: url('<?php the_post_thumbnail_url(); ?>');
                }
            </style>
            <div class="page__header-background"></div>
            <?php
        endif;
        ?>
        <?php
        if ( function_exists('yoast_breadcrumb') ) {
            $breadcrumbs = yoast_breadcrumb('<nav class="breadcrumbs" aria-label="breadcrumbs">', '</nav>', true);
        }    
        the_title( '<h1 class="page__title">', '</h1>' );
        if ( has_excerpt( $post->ID ) ):?>
            <div class="page__excerpt"><?php the_excerpt(); ?></div>
        <?php endif; ?>
	</header><!-- .entry-header -->

	<?php
    // whatchagot_loran_post_thumbnail();
    ?>

	<div class="entry__content content">
		<?php
		the_content();

		wp_link_pages( array(
			'before' => '<nav class="entry__page-links">' . esc_html__( 'Pages:', 'whatchagot-loran' ),
			'after'  => '</nav>',
            'link_before'      => '',
            'link_after'       => '',
		) );
		?>
	</div><!-- .entry-content -->

	<?php if ( get_edit_post_link() ) : ?>
		<footer class="entry__footer">
			<?php
			edit_post_link(
				sprintf(
					wp_kses(
						/* translators: %s: Name of current post. Only visible to screen readers */
						__( 'Edit <span class="is-sr-only">%s</span>', 'whatchagot-loran' ),
						array(
							'span' => array(
								'class' => array(),
							),
						)
					),
					get_the_title()
				),
				'<span class="edit-link">',
				'</span>'
			);
			?>
		</footer><!-- .entry-footer -->
	<?php endif; ?>
</article><!-- #post-<?php the_ID(); ?> -->
