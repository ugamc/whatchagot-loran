<?php
/**
 * Template part for displaying page content in front-page.php
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package Whatchagot_Loran
 */

?>

<header class="front-page__header">
     <h1>
        <?php
        if ( function_exists( 'the_custom_logo' ) ) {
            if ( has_custom_logo() ) {
                $custom_logo_id = get_theme_mod( 'custom_logo' );
                $image = wp_get_attachment_image_src( $custom_logo_id , 'full' );
                ?>
                    <img class="front-page__logo" src="<?php echo $image[0]; ?>" alt="<?php bloginfo( 'name' ); ?>" />
                <?php
            } else {
                bloginfo( 'name' );
            }
        } else {
            bloginfo( 'name' );
        }
        ?>
    </h1>
</header>

<article id="post-<?php the_ID(); ?>" <?php post_class( 'entry front-page' ); ?>>
    <div class="columns">
        <div class="column">
            <?php
            the_content();
            ?>
        </div>
        
        <div class="column is-5 is-offset-1-desktop">
            <nav>
                <?php
                
                wp_nav_menu( array(
                    'theme_location' => 'menu-1',
                    'menu_class'        => 'front-page__menu',
                    'depth'          => 1
                ));
                
                ?>
            </nav>
        </div>
    </div>
</article><!-- #post-<?php the_ID(); ?> -->
