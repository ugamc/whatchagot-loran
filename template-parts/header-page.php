<?php
/**
 * Template part for displaying page header in page.php
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package Whatchagot_Loran
 */

?>

<header id="masthead" class="site__header">
    <div id="header-content" class="site__header-content">
        <div class="site__branding">
            <form class="site__search" action="/" method="get">
                <div class="field">
                    <label class="label is-sr-only" for="search">Search <?php bloginfo( 'name' ); ?></label>
                    <div class="control has-icons-left">
                        <input class="input" type="text" name="s" id="search" autocomplete="off" value="<?php the_search_query(); ?>" />
                        <span class="icon is-small is-left">
                            <i class="fas fa-search"></i>
                        </span>
                    </div>
                </div>
            </form>
            <?php

            if ( is_front_page() && is_home() ) :
                ?>
                <h1 class="site__title">
                    <a href="<?php echo esc_url( home_url( '/' ) ); ?>" rel="home">
                        <?php
                            if ( function_exists( 'the_custom_logo' ) ) {
                                if ( has_custom_logo() ) {
                                    $custom_logo_id = get_theme_mod( 'custom_logo' );
                                    $image = wp_get_attachment_image_src( $custom_logo_id , 'full' );
                                    ?>
                                        <img src="<?php echo $image[0]; ?>" alt="<?php bloginfo( 'name' ); ?>" />
                                    <?php
                                } else {
                                    bloginfo( 'name' );
                                }
                            } else {
                                bloginfo( 'name' );
                            }
                        ?>
                    </a>
                </h1>
                <?php
            else :
                ?>
                <div class="site__title">
                    <a href="<?php echo esc_url( home_url( '/' ) ); ?>" rel="home">
                        <?php
                            if ( function_exists( 'the_custom_logo' ) ) {
                                if ( has_custom_logo() ) {
                                    $custom_logo_id = get_theme_mod( 'custom_logo' );
                                    $image = wp_get_attachment_image_src( $custom_logo_id , 'full' );
                                    ?>
                                        <img src="<?php echo $image[0]; ?>" alt="<?php bloginfo( 'name' ); ?>" />
                                    <?php
                                } else {
                                    bloginfo( 'name' );
                                }
                            } else {
                                bloginfo( 'name' );
                            }
                        ?>
                    </a>
                </div>
                <?php
            endif;
            $whatchagot_loran_description = get_bloginfo( 'description', 'display' );
            if ( $whatchagot_loran_description || is_customize_preview() ) :
                ?>
                <p class="site__description"><?php echo $whatchagot_loran_description; /* WPCS: xss ok. */ ?></p>
            <?php endif; ?>

        </div><!-- .site__branding -->

        <nav id="main-navigation" class="site__navigation">
            <?php
            wp_nav_menu( array(
                'theme_location' => 'menu-1',
                'menu_id'        => 'primary-menu',
            ) );
            ?>
        </nav><!-- #site__navigation -->
    </div>
    <div class="site__mobile-nav">
        <div class="buttons">
            <!--<button>
                <span class="icon">
                    <i class="fas fa-fw fa-chevron-up"></i>
                </span>
            </button>-->
            <button id="mobile-toggle">
                <span class="icon">
                    <i class="fas fa-bars"></i>
                </span>
            </button>
            <!--<button>
                <span class="icon">
                    <i class="fas fa-chevron-down"></i>
                </span>
            </button>-->
        </div>
    </div>
</header><!-- #masthead -->
