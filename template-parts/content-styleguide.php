<?php
/**
 * Template part for displaying posts
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package Whatchagot_Loran
 */

?>

<article id="post-<?php the_ID(); ?>" <?php post_class( 'entry' ); ?>>
	<header class="page__header">
		<?php
		if ( is_singular() ) :
            if ( function_exists('yoast_breadcrumb') ) {
	            $breadcrumbs = yoast_breadcrumb('<nav class="breadcrumbs" aria-label="breadcrumbs">', '</nav>', true);
	        }
            the_title( '<h1 class="page__title">', '</h1>' );
		else :
			the_title( '<h2 class="entry__title"><a href="' . esc_url( get_permalink() ) . '" rel="bookmark">', '</a></h2>' );
		endif;

		if ( 'post' === get_post_type() ) :
			?>
			<div class="entry__meta">
				<?php
				whatchagot_loran_posted_on();
				whatchagot_loran_posted_by();
				?>
			</div><!-- .entry-meta -->
		<?php endif; ?>
	</header><!-- .entry-header -->

	<div class="entry__content content">
        <?php
        if ( get_field('pronunciation') ) {
            printf( '<div class="styleguide__pronunciation"><span class="is-sr-only">Pronunciation</span>%s</div>', get_field('pronunciation') );
        }
        
		the_content( sprintf(
			wp_kses(
				/* translators: %s: Name of current post. Only visible to screen readers */
				__( 'Continue reading<span class="screen-reader-text"> "%s"</span>', 'whatchagot-loran' ),
				array(
					'span' => array(
						'class' => array(),
					),
				)
			),
			get_the_title()
		) );

		if ( get_field('second_reference') ) {
			printf( '<div class="notification is-light"><strong>On Second Reference</strong>%s</div>', get_field('second_reference') );
		}

		if ( get_field('more_information_link') ) {
			printf( '<p class="buttons"><a class="button is-link is-outlined" href="%s">More Information</a></p>', esc_url( get_field('more_information_link') ) );
		}
		
		?>
	</div><!-- .entry-content -->
    
    <?php
        wp_link_pages( array(
			'before' => '<nav class="entry__page-links"><ul class="entry__page-links-list"><li>',
			'after'  => '</li></ul></nav>',
            // 'link_before'      => '<li>',
            // 'link_after'       => '</li>',
            'separator'        => '</li><li>'
		) );
    ?>

	<footer class="entry__footer">
		<?php whatchagot_loran_entry_footer(); ?>
	</footer><!-- .entry-footer -->
</article><!-- #post-<?php the_ID(); ?> -->
