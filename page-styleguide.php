<?php
/**
 * The template for displaying all pages
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site may use a
 * different template.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package Whatchagot_Loran
 */

get_header();
?>
    <div class="section">
        <div id="site__content-area" class="container">
            <main id="primary" class="site__main">
                <?php
                while ( have_posts() ) :
                    the_post();

                    get_template_part( 'template-parts/content', 'page' );

                    // If comments are open or we have at least one comment, load up the comment template.
                    if ( comments_open() || get_comments_number() ) :
                        comments_template();
                    endif;

                endwhile; // End of the loop.
                ?>
                <?php
                $terms = get_terms( array(
                    'taxonomy' => 'styleguide_categories',
                    'hide_empty' => false,
                ) );
                if ( $terms ) {
                    ?>
                    <div class="columns" id="styleguide__list-filter">
                        <div class="column is-narrow">
                            <div id="styleguide-categories" class="dropdown">
                                <div class="dropdown-trigger">
                                    <button class="button is-light" aria-haspopup="true" aria-controls="categories-menu">
                                        <span class="dropdown-label">Filter by Category</span>
                                        <span class="icon is-small">
                                            <i class="fas fa-angle-down" aria-hidden="true"></i>
                                        </span>
                                    </button>
                                </div>
                                <div class="dropdown-menu" id="categories-menu" role="menu">
                                    <div class="dropdown-content">
                                        <?php
                                        foreach ( $terms as $term ) :
                                            printf( '<a href="%2$s" class="dropdown-item" data-filter="%3$s">%1$s</a>', esc_html($term->name), get_category_link( $term->term_id ), esc_attr($term->slug) ); // WPCS: XSS OK.
                                        endforeach;
                                        ?>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="column">
                            <div class="field">
                                <div class="control has-icons-left is-expanded">
                                    <input class="input" type="text" id="styleguide__list-search" placeholder="Filter by Title" autocomplete="off" value="<?php the_search_query(); ?>" />
                                    <span class="icon is-small is-left">
                                        <i class="fas fa-search"></i>
                                    </span>
                                </div>
                            </div>
                        </div>
                    </div>
                    <?php
                }

                // Custom Post Query 
                $args = array (
                    'post_type' => 'styleguide',
                    'order' => 'ASC',
                    'orderby' => 'title',
                    'posts_per_page' => '1000'
                );

                $the_query = new WP_Query($args);

                while( $the_query->have_posts() ) : $the_query->the_post();
                    $categories_list = wp_get_post_terms( get_the_ID(), array( 'styleguide_categories' ) );
                    $categories_output = "";
                    if ( $categories_list ) {
                        foreach ( $categories_list as $term ) :
                            $categories_output .= sprintf( wp_kses( $term->slug, array() )  . " " );
                        endforeach;
                        $categories_output = trim($categories_output);
                    }
                    if ($the_query->current_post == 0):
                        ?>
                        <ul class="styleguide__list">
                        <?php
                    endif;
                ?>
                <li class="styleguide__list-entry <?php echo esc_attr($categories_output); ?>" data-filter="<?php echo esc_attr($categories_output); ?>">
                    <?php
                    the_title( '<a class="styleguide__list-link" href="' . esc_url( get_permalink() ) . '" rel="bookmark">', '</a>' );
                    ?>
                </li>
                <?php
                if (($wp_query->current_post +1) == ($wp_query->post_count)):
                    ?>
                    </ul>
                    <?php
                endif;
                ?>
        
                <?php
                endwhile;
                wp_reset_postdata();
                ?>
            </main><!-- #primary -->
            <?php
            get_sidebar();
            ?>
        </div><!-- #site__content-area -->
    </div><!-- .section -->

<?php
get_footer();
