<?php
/**
 * The template for displaying all single posts
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#single-post
 *
 * @package Whatchagot_Loran
 */

get_header();
?>

	<div class="section">
        <div id="site__content-area" class="container">
            <main id="primary" class="site__main">

                <?php
                while ( have_posts() ) :
                    the_post();

                    get_template_part( 'template-parts/content', get_post_type() );

                    // the_post_navigation();

                    // If comments are open or we have at least one comment, load up the comment template.
                    if ( comments_open() || get_comments_number() ) :
                        comments_template();
                    endif;

                endwhile; // End of the loop.
                ?>

            </main><!-- #primary -->
            <?php
            get_sidebar();
            ?>
        </div><!-- #site__content-area -->
    </div><!-- .section -->

<?php
get_footer();