<?php
/**
 * The template for displaying 404 pages (not found)
 *
 * @link https://codex.wordpress.org/Creating_an_Error_404_Page
 *
 * @package Whatchagot_Loran
 */

get_header();
?>

    <div class="section">
        <div id="site__content-area" class="container">
            <main id="primary" class="site__main">

                <section class="error-404 not-found">
                    <header class="page__header">
                        <h1 class="page__title"><?php esc_html_e( '404: Page Not Found', 'whatchagot-loran' ); ?></h1>
                    </header><!-- .page-header -->

                    <div class="entry__content content">
                        <p><?php esc_html_e( 'It looks like this link is broken or the page has been moved.', 'whatchagot-loran' ); ?></p>
                        <?php
                        //get_search_form();
                        ?>
                        
                        <form class="site__search" action="/" method="get">
                            <div class="field has-addons">
                                <div class="control has-icons-left is-expanded">
                                    <input class="input" type="text" name="s" id="search" placeholder="What are you looking for?" autocomplete="off" value="<?php the_search_query(); ?>" />
                                    <span class="icon is-small is-left">
                                        <i class="fas fa-search"></i>
                                    </span>
                                </div>
                                <div class="control">
                                    <button type="submit" class="button is-primary">Search</a>
                                </div>
                            </div>
                        </form>

                    </div><!-- .page-content -->
                </section><!-- .error-404 -->
            </main>
            
        </div><!-- #site__content-area -->
    </div><!-- .section -->

<?php
get_footer();
