'use strict';

import plugins  from 'gulp-load-plugins';
import gulp     from 'gulp';
import yaml     from 'js-yaml';
import fs       from 'fs';

// Prepare and optimize code etc
import autoprefixer from 'autoprefixer';
import browserSync  from 'browser-sync';

// Name of working theme folder
// const root = '../' + themename + '/',
const root = './';
const scss = root + 'sass/';
const js = root + 'js/';
const img = root + 'images/';
const languages = root + 'languages/';

// load all installed Gulp modules
// if module name is "gulp-myplugin", load as $.myplugin
const $ = plugins();

// Load settings from config.yml
const { COMPATIBILITY, PORT, PATHS } = loadConfig();

function loadConfig() {
	let ymlFile = fs.readFileSync('config.yml', 'utf8');
	return yaml.load(ymlFile);
}

// CSS via Sass and Autoprefixer
gulp.task('css', function() {
	return gulp.src(scss + '{style.scss,rtl.scss}')
	.pipe($.sourcemaps.init())
	.pipe($.sass({
        // includePaths: PATHS.sass,
        includePaths: [ root + 'node_modules/bulma/sass'],
		outputStyle: 'expanded', 
		indentType: 'tab',
		indentWidth: '1'
	}).on('error', $.sass.logError))
	.pipe($.postcss([
		autoprefixer({
            browsers: COMPATIBILITY
        })
	]))
	.pipe($.sourcemaps.write(scss + 'maps'))
	.pipe(gulp.dest(root));
});

// Optimize images through gulp-image
gulp.task('images', function() {
	return gulp.src(img + 'RAW/**/*.{jpg,JPG,png}')
	.pipe($.newer(img))
	.pipe($.image())
	.pipe(gulp.dest(img));
});

// JavaScript
gulp.task('javascript', function() {
	return gulp.src([js + '*.js'])
	.pipe($.jshint())
	.pipe($.jshint.reporter('default'))
	.pipe(gulp.dest(js));
});


// Watch everything
gulp.task('watch', function() {
	browserSync.init({ 
		open: 'external',
		proxy: 'brand.vm',
		port: PORT
	});
	gulp.watch([root + 'sass/**/*.css', root + 'sass/**/*.{sass,scss}' ], ['css']);
	gulp.watch(js + '**/*.js', ['javascript']);
	gulp.watch(img + 'RAW/**/*.{jpg,JPG,png}', ['images']);
	gulp.watch(root + '**/*.{php,css,jpg,JPG,png,js}').on('change', browserSync.reload);
});


// Default task (runs at initiation: gulp --verbose)
gulp.task('default', ['watch']);
