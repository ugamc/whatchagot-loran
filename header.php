<?php
/**
 * The header for our theme
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package Whatchagot_Loran
 */

?>
<!doctype html>
<html <?php language_attributes(); ?>>
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="profile" href="https://gmpg.org/xfn/11">
    
    <meta name="application-name" content="<?php bloginfo( 'name' ); ?>"/>

	<!-- Chrome for Android -->
	<meta name="theme-color" content="#ba0c2f" />

	<!-- iOS icons -->
	<link rel="apple-touch-icon-precomposed" sizes="57x57" href="<?php echo get_template_directory_uri(); ?>/icons/apple-touch-icon-57x57.png" />
	<link rel="apple-touch-icon-precomposed" sizes="114x114" href="<?php echo get_template_directory_uri(); ?>/icons/apple-touch-icon-114x114.png" />
	<link rel="apple-touch-icon-precomposed" sizes="72x72" href="<?php echo get_template_directory_uri(); ?>/icons/apple-touch-icon-72x72.png" />
	<link rel="apple-touch-icon-precomposed" sizes="144x144" href="<?php echo get_template_directory_uri(); ?>/icons/apple-touch-icon-144x144.png" />
	<link rel="apple-touch-icon-precomposed" sizes="60x60" href="<?php echo get_template_directory_uri(); ?>/icons/apple-touch-icon-60x60.png" />
	<link rel="apple-touch-icon-precomposed" sizes="120x120" href="<?php echo get_template_directory_uri(); ?>/icons/apple-touch-icon-120x120.png" />
	<link rel="apple-touch-icon-precomposed" sizes="76x76" href="<?php echo get_template_directory_uri(); ?>/icons/apple-touch-icon-76x76.png" />
	<link rel="apple-touch-icon-precomposed" sizes="152x152" href="<?php echo get_template_directory_uri(); ?>/icons/apple-touch-icon-152x152.png" />

	<!-- Favicon -->
	<link rel="icon" type="image/png" href="<?php echo get_template_directory_uri(); ?>/icons/favicon-196x196.png" sizes="196x196" />
	<link rel="icon" type="image/png" href="<?php echo get_template_directory_uri(); ?>/icons/favicon-192.png" sizes="192x192" />
	<link rel="icon" type="image/png" href="<?php echo get_template_directory_uri(); ?>/icons/favicon-96x96.png" sizes="96x96" />
	<link rel="icon" type="image/png" href="<?php echo get_template_directory_uri(); ?>/icons/favicon-32x32.png" sizes="32x32" />
	<link rel="icon" type="image/png" href="<?php echo get_template_directory_uri(); ?>/icons/favicon-16x16.png" sizes="16x16" />
	<link rel="icon" type="image/png" href="<?php echo get_template_directory_uri(); ?>/icons/favicon-128.png" sizes="128x128" />

	<?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>
    
<a class="skip-link is-sr-only" href="#content"><?php esc_html_e( 'Skip to content', 'whatchagot-loran' ); ?></a>

<header class="global-header">
    <a class="global-header__logo-link" href="https://www.uga.edu/">
        University of Georgia
    </a>
    <nav class="global-header__nav">
        <div class="is-block-touch is-hidden-desktop">
            <a class="button is-small is-light">
                <!--<span>UGA Links</span>-->
                <span class="icon is-small">
                    <i class="fas fa-chevron-down"></i>
                </span>
            </a>
        </div>
        <div class="global-header__menus">
            <?php
                wp_nav_menu( array(
                    'theme_location' => 'menu-2',
                    'menu_id'        => 'global-header__links',
                    'menu_class'     => 'global-header__links global-header__resource-links'
                ) );
            ?>
            <?php
                wp_nav_menu( array(
                    'theme_location' => 'social-links',
                    'menu_id'        => 'global-header__social-links',
                    'menu_class'     => 'global-header__links global-header__social-links'
                ) );
            ?>
            <a class="is-hidden" href=""><i class="fas fa-fw fa-search"></i><span class="is-sr-only">Search</span></a>
        </div>
    </nav>
   
</header>

<div id="page" class="site">
    <?php
    if ( is_front_page() ) :
        get_template_part( 'template-parts/header', 'front' );
    else:
        get_template_part( 'template-parts/header', 'page' );
    endif;
    ?>

	<div id="content-wrapper" class="site__content-wrapper">
        <div id="content" class="site__content">
